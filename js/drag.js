/*
	实现div随鼠标拖拽(面对对象的)
		function Drag(id)
	实现div随鼠标拖拽
		function drag(id)
	实现div随鼠标拖拽,限制在窗口内(面对对象)
		function LimitDrag(id)
 */


/*
	实现div随鼠标移动实现拖拽效果
	div需指定宽高, position:absolute;
	参数:
		id: div的id
	示例: 
		new Drag('div1');
*/
function Drag(id){
	var _this=this;
	
	this.disX=0;
	this.disY=0;
	this.oDiv=document.getElementById(id);
	// 鼠标在div中按下,调用方法
	this.oDiv.onmousedown=function (event)	{
		_this.fnDown(event);
	};
}
// 鼠标按下时调用的方法
Drag.prototype.fnDown=function (ev){
	var _this=this;
	var oEvent=ev||event;
	// 保存鼠标在div的位置
	this.disX=oEvent.clientX-this.oDiv.offsetLeft;
	this.disY=oEvent.clientY-this.oDiv.offsetTop;
	
	document.onmousemove=function (){
		_this.fnMove();
	};
	
	document.onmouseup=function (){
		_this.fnUp();
	};
	// 阻止默认事件,防止移动过程中会选中内容
	oEvent.preventDefault();
};
// 鼠标移动时调用的方法
Drag.prototype.fnMove=function (ev){
	var oEvent=ev||event;
	// 修改div的位置
	this.oDiv.style.left=oEvent.clientX-this.disX+'px';
	this.oDiv.style.top=oEvent.clientY-this.disY+'px';
};
// 鼠标松开时调用的方法
Drag.prototype.fnUp=function (){
	document.onmousemove=null;
	document.onmouseup=null;
};

/*
	实现div随鼠标移动实现拖拽效果
	div需指定宽高, position:absolute;
	参数:
		id: div的id
	示例: 
		drag('div1');
*/
function drag(id){
	var div = document.getElementById(id);
	div.onmousedown = function(ev){
		var oEvent = ev||event;
		// 保存鼠标与div的相对位置
		var disX = oEvent.clientX-div.offsetLeft;
		var disY = oEvent.clientY-div.offsetTop;
		// 鼠标移动
		document.onmousemove = function(ev){
			var oEvent = ev||event;
			div.style.left = oEvent.clientX-disX + 'px';
			div.style.top = oEvent.clientY-disY + 'px';
		};
		document.onmouseup = function(){
			document.onmousemove = null;
			document.onmouseup = null;
		}
		oEvent.preventDefault();
	};
	
}





/*
	实现div随鼠标移动实现拖拽效果, 会限制移动范围,防止移出窗口, 继承了 Drag 类
	div需指定宽高, position:absolute;
	参数:
		id: div的id
	示例: 
		new LimitDrag('div1');
*/
function LimitDrag(id){
	Drag.call(this, id);
}

for(var i in Drag.prototype){
	LimitDrag.prototype[i]=Drag.prototype[i];
}
// 重写父类的移动方法,使其不会移出窗口
LimitDrag.prototype.fnMove=function (ev){
	var oEvent=ev||event;
	var l=oEvent.clientX-this.disX;
	var t=oEvent.clientY-this.disY;
	
	if(l<0){
		l=0;
	}
	else if(l>document.documentElement.clientWidth-this.oDiv.offsetWidth){
		l=document.documentElement.clientWidth-this.oDiv.offsetWidth;
	}
	
	if(t<0){
		t=0;
	}
	else if(t>document.documentElement.clientHeight-this.oDiv.offsetHeight){
		t=document.documentElement.clientHeight-this.oDiv.offsetHeight;
	}
	
	this.oDiv.style.left=l+'px';
	this.oDiv.style.top=t+'px';
};