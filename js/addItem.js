/*
导入之前需要先导入jQuery

example:

HTML部分:
    <button id="button">添加</button>
    <!-- 放内容的div -->
    <div id="content_div">
        <div id="example">
            <button remove>删除</button>
            <button add>添加</button>
            <lable>姓名</lable>
            <input type="text" class="name" name="name">
        </div>
    </div>
js部分:
    new AddItem({
        // 存放条目内容的div
        'content_div_id': 'content_div',
        // 每条添加的div例子
        'example_div_id': 'example',
        // 点击添加的按钮
        'add_button_id': 'button',
    }).run();
 */



/**
 * 用于添加条目, 不定数输入框, 在每个input的name后拼接当前序号
 * 每个item中, 有remove属性的是删除当前条目按钮
 * 每个item中, 有add属性的是在当前item后添加条目按钮
 * @param params
 * {
 *     'content_div_id': 存放内容条目的divID, 默认 content_div_id
 *     'example_div_id': 添加的div例子ID, 默认 example_div_id
 *     'add_button_id': 点击添加的按钮ID, 默认 add_button_id
 *     'add_success': 添加成功的回调函数, 参数为当前itemDIV jQuery对象 序号 当前添加的item个数 默认空
 *     'start_num': 起始长度, 默认为0,
 *     'max_num': 可添加的最大个数, 默认无限
 * }
 * @constructor
 */
function AddItem(params) {
    // 接收参数
    var contentDivId = params['content_div_id'] || 'content_div_id';
    var exampleDivId = params['example_div_id'] || 'example_div_id';
    var addButtonId = params['add_button_id'] || 'add_button_id';

    this.addButton = $('#' + addButtonId);
    this.contentDiv = $('#' + contentDivId);
    this.exampleDiv = $('#' + exampleDivId);
    this.addSuccessFunction = params['add_success'];
    this.startNum = params['start_num'] || 0;
    this.maxNum = params['max_num'] || -1;
    this.secp = 0;
    // 保存当前已经添加的数量
    this.num = 0;
}


/**
 * 运行函数
 */
AddItem.prototype.run = function () {
    var _this = this;
    this.addButton.click(function (event) {
        _this.addFistItem();
        event.preventDefault();
    });
    // 删除示例div
    this.exampleDiv.remove();
    // 删除div的id
    this.exampleDiv.removeAttr('id');
    // 添加初始个数
    for(let i = 0; i < this.startNum; i++){
        this.addLastItem();
    }
};

// 向内容div的第一个添加
AddItem.prototype.addFistItem = function () {
    // 判断是否超出最大数量
    if(this.maxNum != -1 && this.num >= this.maxNum) return;
    var divItem = this.getDivItem();
    // 添加
    this.contentDiv.prepend(divItem);
    // 调用回调函数
    if (this.addSuccessFunction) this.addSuccessFunction(divItem, this.secp, ++this.num);
    // 序号迭代
    this.secpIter();
};

// 向内容div的最后一个添加
AddItem.prototype.addLastItem = function () {
    // 判断是否超出最大数量
    if(this.maxNum != -1 && this.num >= this.maxNum) return;
    var divItem = this.getDivItem();
    // 添加
    this.contentDiv.apend(divItem);
    // 调用回调函数
    if (this.addSuccessFunction) this.addSuccessFunction(divItem, this.secp, ++this.num);
    // 序号迭代
    this.secpIter();
};

// 向元素后面添加
AddItem.prototype.addAfterItem = function(item) {
    // 判断是否超出最大数量
    if(this.maxNum != -1 && this.num >= this.maxNum) return;
    var divItem = this.getDivItem();
    item.after(divItem);
    // 调用回调函数
    if (this.addSuccessFunction) this.addSuccessFunction(divItem, this.secp, ++this.num);
    // 序号迭代
    this.secpIter();
};

// 获取当前序号的div
AddItem.prototype.getDivItem = function () {
    var cloneDiv = this.exampleDiv.clone();
    var secp = this.secp;
    // 将div的所有 input 的name加上当前序号
    cloneDiv.find('input').each(function () {
        var name = $(this).attr('name');
        $(this).attr('name', name + '_' + secp);
    });
    var _this = this;
    // 给添加按钮添加点击事件
    cloneDiv.find('[add]').click(function (event) {
        _this.addAfterItem(cloneDiv);
        event.preventDefault();
    });
    // 给删除按钮添加点击事件
    cloneDiv.find('[remove]').click(function (event) {
        cloneDiv.remove();
        // 条目-1
        _this.num--;
        event.preventDefault();
    });
    return cloneDiv;
};

// 序号向后延展
AddItem.prototype.secpIter = function () {
    this.secp += 1;
};