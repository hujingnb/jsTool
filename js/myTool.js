/*
方法列表:
	ajax请求
		function ajax(url, fnSucc, fnFaild)
	获取元素最终样式
		function getStyle(obj, attr)
	获取或设置css属性
		function css(obj, attr, value)
	阻止右键菜单
		function stopRightMenu()
	字符串函数
		去除字符串两边的空格
			function delStrSpace(str)
		验证字符串是否为空(会去除两边的空格)
			function isNull(str)
		关键词高亮显示
			function keyWordsHighlight(e, keys, color)
		获取随机颜色
			function getRandomColor()		
	时间函数
		返回当前时间字符串
			function getNowTimeStr(a, b)
		返回到指定日期还剩多少天,数字
			function getCountDown(Y, M, D, h, m, s)
		将字符串转成日期对象
			getDate(dateStr)
		根据服务器时间动态获取当前时间
			NowDate	对象
    文件相关
		导出文件到本地
            function exportRawFile(fileName, fileData)
*/



/**
 * 原生ajax封装
 * @param url
 * 请求的资源url
 * @param fnSucc
 * 成功时的回调
 * @param fnFaild
 * 失败时的回调
 */
function ajax(url, fnSucc, fnFaild){
	var xmlHttp = null;
	// 创建ajax 对象
	if(window.XMLHttpRequest){
		xmlHttp = new XMLHttpRequest();
	}else{
		xmlHttp = new ActiveXObject("Msxml2.XMLHTTP") || new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlHttp.open('GET', url, true);
	// 发送请求
	xmlHttp.send(null);
	// ajax状态
	xmlHttp.onreadystatechange = function(){
		// 通信完成
		if(xmlHttp.readyState == 4){
			// 是否成功
			if(xmlHttp.status == 200){
				fnSucc(xmlHttp.responseText);
			}else{
				if(fnFaild){
					fnFaild();
				}
			}
		}
	}
}


/**
 * 获取元素的最终样式
 * @param obj
 * 元素(Element)
 * @param attr
 * 属性
 * @returns {string|*}
 */
function getStyle(obj, attr){
	if(obj.currentStyle){
		return obj.currentStyle[attr];
	}
	else{
		return getComputedStyle(obj, false)[attr];
	}
}

/**
 * 阻止右键菜单
 */
function stopRightMenu(){
	document.oncontextmenu=function(){
		return false;
	}
}

/**
 * 原生设置或返回css属性
 * 可以连续调用,例: css(div,'width',100)('height',200);
 * @param obj
 * 元素
 * @param attr
 * 属性名
 * @param value
 * 要设置的值，无需单位，若不设置不给即可
 * @returns {number|string|(function(...[*]=))}
 */
function css(obj, attr, value){
	// 当参数为两个是返回属性值
	if(arguments.length==2){
		if(attr=="transform"){
			return obj.transform;
		}
		var i=parseFloat(obj.currentStyle?obj.currentStyle[attr]:document.defaultView.getComputedStyle(obj, false)[attr]);
		var val=i?i:0;
		if(attr=="opacity"){
			val*=100;
		}
		return val;
	// 当参数为三个时设置属性
	}else if(arguments.length==3){
		switch(attr){
			case 'width':
			case 'height':
			case 'paddingLeft':
			case 'paddingTop':
			case 'paddingRight':
			case 'paddingBottom':
				value=Math.max(value,0);
			case 'left':
			case 'top':
			case 'marginLeft':
			case 'marginTop':
			case 'marginRight':
			case 'marginBottom':
				obj.style[attr]=value+'px';
				break;
			case 'opacity':
				if(value<0){
					value=0;
				}
				obj.style.filter="alpha(opacity:"+value+")";
				
				obj.style.opacity=value/100;
				break;
			case 'transform':
				obj.transform=value;
				obj.style["transform"]="rotate("+value+"deg)";
				obj.style["MsTransform"]="rotate("+value+"deg)";
				obj.style["MozTransform"]="rotate("+value+"deg)";
				obj.style["WebkitTransform"]="rotate("+value+"deg)";
				obj.style["OTransform"]="rotate("+value+"deg)";
			break;
			default:
				obj.style[attr]=value;
		}
		return function (attr_in, value_in){
			css(obj, attr_in, value_in)
		};
	}
}

/**
 * 去除字符串两边的空格
 * @param str
 * @returns {void | string | *}
 */
function delStrSpace(str){
	return str.replace( /^(\s|\u00A0)+|(\s|\u00A0)+$/g, "" );//正则替换
}

/**
 * 验证字符串是否为空(会去除两边的空格)
 * @param str
 * @returns {boolean}
 */
function isNull(str){
	if(!delStrSpace(str)){
		return true;
	}else{
		return false;
	}
}

/**
 * 关键词高亮显示
 * @param e
 * 元素，如p标签
 * @param keys
 * 关键词数组
 * @param color
 * 显示的颜色，默认黑色
 */
function keyWordsHighlight(e, keys, color){
	var
		i = 0,
		l = keys.length,//关键词的长度
		k = "";
	for(; i < l ; i++){
		k = keys[i];//获取关键词的对象
		//替换关键词的数据
		e.innerHTML = e.innerHTML.replace(k, "<span style='color:"+ (color || "#000")+"'>" + k + "</span>")
	}
}

/**
 * 返回当前时间字符串(无参或只传一个参数,会使用中文分割)
 * @param a
 * 分割年月日的符号
 * @param b
 * 分割时分秒的符号
 * @returns {string}
 */
function getNowTimeStr(a, b){
	var date =new Date();//获取日期对象
	
	if(a && b){
		return date.getFullYear() + a
				+ (date.getMonth() + 1) + a
				+ date.getDate()
				+ " "
				+ date.getHours() + b
				+ date.getMinutes() + b
				+ date.getSeconds();
		
	}else{
		/*获取年、月、日、时、分、秒，本地系统的时间*/
		return date.getFullYear() + "年"
				+ (date.getMonth() + 1) + "月"
				+ date.getDate() + "日"
				+ " "
				+ date.getHours() + "时"
				+ date.getMinutes() + "分"
				+ date.getSeconds() + "秒";
	}
	
}

/**
 * 返回当前时间距离指定日期还剩多少天
 * @param Y 年(默认0)
 * @param M 月(默认0)
 * @param D 日(默认0)
 * @param h 时(默认0)
 * @param m 分(默认0)
 * @param s 秒(默认0)
 * @returns {number}
 */
function getCountDown(Y, M, D, h, m, s){
	Y = Y || 0;
	M = M || 0;
	D = D || 0;
	h = h || 0;
	m = m || 0;
	s = s || 0;
	var date = new Date(Y, M-1, D, h, m, s),
	//转换为时间戳，方便计算差值
	times = date.getTime() - new Date().getTime();
	//返回天数
	return Math.ceil(times / (1000 * 60 * 60 * 24));
}

/**
 * 将时间字符串转换成date对象
 * @param dateStr
 * 时间字符串
 */
function getDate(dateStr){
	/* 若日期是使用-分割的，全部转换成/
        因为只有日期时，js会将-分割的字符串基准时区设置为GMT，与当前时区相差8小时 */
	dateStr = dateStr.replace(/-/g, '/');
	return new Date(dateStr);
}


/**
 * 获取当前时间的对象
 * @param serverTime
 * 服务器时间戳（秒）
 */
function GetNowDate(serverTime){
	this.serverTime = serverTime || new Date().getTime();
	// 记录经历时间
	this.experienceTime = 0;
	// 记录创建对象时的时间
	this.createTime = new Date();
	// 记录的时间与客户端时间最大差值（秒）
	this.maxErrorTimeDiff = 20;
	this.init();
}

/**
 * 获取当前时间戳，毫秒级
 */
GetNowDate.prototype.getTime = function (){
	var time = this.getDateObject();
	if(!time) return null;
	return time.getTime();
};

/**
 * 获取当前时间date对象
 */
GetNowDate.prototype.getDateObject = function(){
	var t = this.getNowMilliTime();
	if(!t) return null;
	return new Date(t);
};

// 获取当前毫秒级时间戳
GetNowDate.prototype.getNowMilliTime = function(){
	var result = new Date().getTime() - this.createTime.getTime();
	// 若时间差与记录时间相差太大，返回null
	if(result > this.experienceTime * 1000 + this.maxErrorTimeDiff) return null;
	return this.serverTime * 1000 + result;
};

// 初始化对象
GetNowDate.prototype.init = function(){
	// 设置定时器，动态增加时间
	var _this = this;
	setInterval(function() {
		_this.experienceTime++;
	}, 1000);
};

/**
 * 返回一个随机的颜色字符串
 * @returns {string}
 */
function getRandomColor(){
	return '#' + Math.floor(Math.random() * 0xffffff).toString(16).padEnd(6, '0');
}

/**
 * 导出文件到本地, 客户端下载
 * @param fileName
 * 文件名
 * @param fileDate
 * 文件内容
 */
function exportRawFile(fileName, fileDate) {
    var urlObject = window.URL || window.webkitURL || window;
    var exportBlob = new Blob([fileDate]);
    var saveLink = document.createElementNS("http://www.w3.org/1999/xhtml", "a");
    saveLink.href = urlObject.createObjectURL(exportBlob);
    saveLink.download = fileName;
    var ev = document.createEvent("MouseEvents");
    ev.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    saveLink.dispatchEvent(ev);
}