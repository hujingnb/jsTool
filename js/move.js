/*
使用需要先引入 myTool.js, 使用了 css方法
*/
// 定义类型,运动用
var MOVE_TYPE={
	BUFFER: 1,	/*缓冲运动*/
	FLEX: 2			/*温柔的运动,嘿嘿*/
};
/*
	运动的函数,可以自定义元素状态改变方式
	参数: 
		obj: 元素
		oTarget: css参数   {width:300}
		iType: 运动类型 MOVE_TYPE 中的元素, 默认缓冲运动
		fnCallBack: 运动停止时调用的函数, 可以不给
		fnDuring: 运动中调用的函数,即css属性每改变一次就调用, 可以不给
*/
function startMove(obj, oTarget, iType, fnCallBack, fnDuring){
	var fnMove=null;
	// 清空之前的定时器
	if(obj.timer){
		clearInterval(obj.timer);
	}
	switch(iType){
		case MOVE_TYPE.BUFFER:
			fnMove=doMoveBuffer;
			break;
		case MOVE_TYPE.FLEX:
			fnMove=doMoveFlex;
			break;
		default: 
			fnMove=doMoveBuffer;
	}
	
	obj.timer=setInterval(function (){
		fnMove(obj, oTarget, fnCallBack, fnDuring);
	}, 30);
}
/*
	停止运动
	参数:
		obj: 运动的元素
*/
function stopMove(obj){
	// 清除定时器
	clearInterval(obj.timer);
}
// 缓冲运动函数
function doMoveBuffer(obj, oTarget, fnCallBack, fnDuring){
	// 是否停止
	var bStop=true;
	var attr='';
	// 定义速度
	var speed=0;
	var cur=0;
	// 进行运动
	for(attr in oTarget){
		cur=css(obj, attr);
		if(oTarget[attr]!=cur){
			bStop=false;
			
			speed=(oTarget[attr]-cur)/5;
			speed=speed>0?Math.ceil(speed):Math.floor(speed);
			css(obj, attr, cur+speed);
		}else{
			bSop=false;
		}
	}
	// 调用运动中函数
	if(fnDuring)fnDuring.call(obj);
	
	if(bStop){
		clearInterval(obj.timer);
		obj.timer=null;
		
		if(fnCallBack)fnCallBack.call(obj);
	}
}
// 温柔的运动函数
function doMoveFlex(obj, oTarget, fnCallBack, fnDuring){
	var bStop=true;
	var attr='';
	// 定义速度
	var speed=0;
	var cur=0;
	
	for(attr in oTarget){
		if(!obj.oSpeed)obj.oSpeed={};
		if(!obj.oSpeed[attr])obj.oSpeed[attr]=0;
		cur=css(obj, attr);
		if(Math.abs(oTarget[attr]-cur)>=1 || Math.abs(obj.oSpeed[attr])>=1){
			bStop=false;
			
			obj.oSpeed[attr]+=(oTarget[attr]-cur)/5;
			obj.oSpeed[attr]*=0.7;
			
			css(obj, attr, cur+obj.oSpeed[attr]);
		}
	}
	if(fnDuring)fnDuring.call(obj);
	if(bStop){
		clearInterval(obj.timer);
		obj.timer=null;
		
		if(fnCallBack)fnCallBack.call(obj);
	}
}